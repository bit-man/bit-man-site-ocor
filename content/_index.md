👋👋👋👋👋👋👋👋👋

{{<imagewithtext img="ocor.png" text="Hello!" >}}


Giving the news about some social network [dramatic changes on API pricing](https://arstechnica.com/gadgets/2023/05/reddits-api-pricing-results-in-shocking-20-million-a-year-bill-for-apollo/) or [changing original direction](https://twitterisgoinggreat.com/) and in many cases no respecting our content freedom I've decided to offer all my posts through my site. Hi *Reddit* and, ehem, *Twitter*, I'm talking to you! 👋

It comes with a price and is that had to maintain my own site. Anyway I am currently doing it through [Gitlab](https://docs.gitlab.com/ee/tutorials/hugo/), [Hugo](https://gohugo.io/) and [Blowfish](https://blowfish.page/). Also have to take all my messages in every social network like Twitter, Reddit, you-name-it and convert them
to a format that can be used in my site. That's why I decided to make [our content our rules](https://gitlab.com/bit-man/our-content-our-rules) tool to convert between social network messages and hosted sites formats

{{<imagewithtext img="woz.jpg" text="Let me explain then what decisions I made and how they translate to the tech stack that supports it, and if it fits your needs and way to do things! " >}}

Otherwise you can reach me through my multiple social networks or [project issues](https://gitlab.com/bit-man/our-content-our-rules/-/issues) and code collaboration

For a more technical and detailed explanation take a look at [my own post about it](https://www.bit-man.guru/posts/our-content-our-rules/)
