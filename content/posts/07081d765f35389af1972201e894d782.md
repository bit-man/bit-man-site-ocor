+++
date = '2010-01-24'
slug = '07081d765f35389af1972201e894d782'
tags = ['twitter']
title = 'Perl 5.11.4 is now available! http://www.xray.mpe.mpg.de/mailing-lists/perl5-porters/2010-01/msg00643.html'
+++
Perl 5.11.4 is now available! http://www.xray.mpe.mpg.de/mailing-lists/perl5-porters/2010-01/msg00643.html
