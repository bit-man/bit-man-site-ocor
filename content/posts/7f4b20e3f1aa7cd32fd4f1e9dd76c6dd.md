+++
date = '2018-02-10'
slug = '7f4b20e3f1aa7cd32fd4f1e9dd76c6dd'
tags = ['twitter']
title = '@rmorganti @fedeaz_ Es que hay un doble problema: los que'
+++
@rmorganti @fedeaz_ Es que hay un doble problema: los que lo analizan pensando en la cultura y condiciones actuales (y le pifian) y los que se aprovechan del error y de la necesidad de creer en algo fantástico (y lo monetizan). Unos pecan de inocentes, los otros de vivos.
