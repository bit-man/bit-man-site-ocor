+++
date = '2018-07-09'
slug = 'c866b9ade140c77a9230f5eda65c91a7'
tags = ['twitter']
title = 'Bacteria-powered solar cell converts light to energy, even under overcast'
+++
Bacteria-powered solar cell converts light to energy, even under overcast skies https://t.co/uXpimnXfEO via @ubcnews
