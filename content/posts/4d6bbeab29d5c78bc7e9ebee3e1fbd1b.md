+++
date = '2022-08-21'
slug = '4d6bbeab29d5c78bc7e9ebee3e1fbd1b'
tags = ['twitter']
title = 'Globally, nearly half of deaths due to cancer can be'
+++
Globally, nearly half of deaths due to cancer can be attributable to preventable risk factors, including the three leading risks of: smoking, drinking too much alcohol or having a high body mass index, a new paper suggests
