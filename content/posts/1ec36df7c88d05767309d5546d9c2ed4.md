+++
date = '2020-12-15'
slug = '1ec36df7c88d05767309d5546d9c2ed4'
tags = ['twitter']
title = '👏👏👏 @Apple is planning to implement new anti-tracking features on'
+++
👏👏👏 @Apple is planning to implement new anti-tracking features on iPhone. This would be a major win for privacy!

BUT they’ve delayed the roll-out due to advertiser pressure. Add your name, and tell Apple to stay strong: https://t.co/IkcFsfKoAd via @Mozilla
