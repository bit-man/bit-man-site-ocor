+++
date = '2021-05-08'
slug = 'e8ef0ea3464934be9716c49253678f64'
tags = ['twitter']
title = '📣 @Theirworlds Education Innovation Awards 2021 are NOW OPEN! Are'
+++
📣 @Theirworld's Education Innovation Awards 2021 are NOW OPEN! Are you working hard to make a difference in the lives of vulnerable young people? Apply now to win a £50k grant, 1-1 mentoring and masterclasses! #UnlockInnovation 🎊 Find out more here: https://t.co/TcXw9RGf6N
