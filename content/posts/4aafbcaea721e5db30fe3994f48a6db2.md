+++
date = '2020-10-10'
slug = '4aafbcaea721e5db30fe3994f48a6db2'
tags = ['twitter']
title = 'Facebook Group recommendations pose a major threat in this election'
+++
Facebook Group recommendations pose a major threat in this election season. Join @Mozilla, @AccountableTech and others in telling @Facebook to stop suggesting Groups until election results are certified: https://t.co/aPy54mLdpV #StopGroupRecs
