+++
date = '2023-02-05'
slug = 'd43d87ae6b680eece74685ff0f7bcdff'
tags = ['twitter']
title = 'RT @TwitterDev: Starting February 9, we will no longer support'
+++
RT @TwitterDev: Starting February 9, we will no longer support free access to the Twitter API, both v2 and v1.1. A paid basic tier will be…
