+++
date = '2021-07-10'
slug = '188886ae03f79e4aa40189311d60de07'
tags = ['twitter']
title = 'RT @davefarley77: 💡 Always remember...'
+++
RT @davefarley77: 💡 Always remember...

EVERYONE IS A JUNIOR AT SOMETHING!

Keep learning. It's OK to say 'I don't know'.
