+++
date = '2021-03-26'
slug = 'd020d2fe6a3e59ee0860ddd7cad83793'
tags = ['twitter']
title = '@PregoneroL Excelente momento para buscar nuevos representantes que sean sindicalistas'
+++
@PregoneroL Excelente momento para buscar nuevos representantes que sean sindicalistas reales, que se preocupen más por quienes representan y menos por perpetuarse. Ah, me olvidaba, eso requiere responsabilidad, compromiso y participación de nuestro lado (los trabajadores)
