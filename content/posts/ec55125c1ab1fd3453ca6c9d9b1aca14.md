+++
date = '2022-05-15'
slug = 'ec55125c1ab1fd3453ca6c9d9b1aca14'
tags = ['twitter']
title = 'Firma la petición y di al rey de #ArabiaSaudí que'
+++
Firma la petición y di al rey de #ArabiaSaudí que levante ya las prohibiciones de viajar impuestas a activistas, defensores y defensoras de los derechos humanos y sus familiares para que puedan, de verdad, ser libres #LetThemFly https://t.co/L3COn77ATh via @amnistiaespana
