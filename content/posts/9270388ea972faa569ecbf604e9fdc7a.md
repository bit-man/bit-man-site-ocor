+++
date = '2021-02-12'
slug = '9270388ea972faa569ecbf604e9fdc7a'
tags = ['twitter']
title = '👏👏👏 @Apple is implementing new anti-tracking features on iPhone –'
+++
👏👏👏 @Apple is implementing new anti-tracking features on iPhone – a major win for privacy! For #DataPrivacyDay @Mozilla is sending @Apple a thank you signed by more than 40,000 supporters. Learn more: https://t.co/vnqnPQ1eOB
