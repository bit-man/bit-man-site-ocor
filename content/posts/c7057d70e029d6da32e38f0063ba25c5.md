+++
date = '2022-10-12'
slug = 'c7057d70e029d6da32e38f0063ba25c5'
tags = ['twitter']
title = 'RT @JetBrains_Fleet: Fleet is now available for everyone to try!'
+++
RT @JetBrains_Fleet: Fleet is now available for everyone to try! 🎉 

Take the public preview of Fleet for a spin and tell us what you think…
