+++
date = '2010-04-09'
slug = 'd9c677fd3ebb9e41269cefb79a6b995a'
tags = ['twitter']
title = 'Mozilla Firefox Lorentz, prevents plugin crashes from crashing your browser'
+++
Mozilla Firefox Lorentz, prevents plugin crashes from crashing your browser -- http://www.mozilla.com/en-US/firefox/lorentz/
